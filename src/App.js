const ACTION_KEY = "$";
const UNDEFINED_ACTION = Symbol("UNDEFINED_ACTION");
class CommandImplError extends Error {
    static get(message) {
        return new EvalError(message);
    }
}
class PrimitiveArgument extends null {
    static push() {
        throw CommandImplError.get("not impl");
    }
    static unshift() {
        throw CommandImplError.get("not impl");
    }
    static merge() {
        throw CommandImplError.get("not impl");
    }
    static splice() {
        throw CommandImplError.get("not impl");
    }
    static apply(old, handler) {
        return handler(old);
    }
    static set(old, v) {
        return v;
    }
}
class ArrayArgument extends PrimitiveArgument {
    static push(old, v) {
        return [...old, ...v];
    }
    static unshift(old, v) {
        return [...v, ...old];
    }
    static merge(old, v) {
        return this.push(old, v);
    }
    static splice(old, [args]) {
        const temp = [...old];
        temp.splice(...args);
        return temp;
    }
}
class ObjectArgument extends PrimitiveArgument {
    static merge(old, v) {
        return { ...old, ...v };
    }
}

const findInvoker = (v) => {
    let ClassConstruct;
    switch (typeof v) {
        case "object":
            ClassConstruct = Array.isArray(v) ? ArrayArgument : ObjectArgument;
            break;
        default:
            ClassConstruct = PrimitiveArgument;
            break;
    }
    return ClassConstruct;
};

const isActionProp = (propertyKey) => propertyKey[0] === ACTION_KEY;
const getActionProp = (propertyKey, hasAction = false) =>
    hasAction || isActionProp(propertyKey)
        ? propertyKey.slice(ACTION_KEY.length)
        : UNDEFINED_ACTION;

const evalCommand = (commandName, base, value) =>
    findInvoker(base)[commandName](base, value);
const executeCommand = (state, baseState, command) => {
    for (const key of Object.keys(command)) {
        if (isActionProp(key)) {
            const actionName = getActionProp(key, true);
            const value = command[key];
            state = evalCommand(actionName, baseState, value);
        } else {
            state[key] = executeCommand({}, baseState[key], command[key] || {});
        }
    }
    return state;
};
module.exports = function update(prevState, changes) {
    return executeCommand({ ...prevState }, prevState, changes);
};
